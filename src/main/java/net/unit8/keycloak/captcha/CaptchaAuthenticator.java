package net.unit8.keycloak.captcha;

import org.jboss.resteasy.spi.HttpResponse;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.FlowStatus;
import org.keycloak.authentication.authenticators.browser.UsernamePasswordForm;
import org.keycloak.common.util.ServerCookie;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.AuthenticatorConfigModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.services.messages.Messages;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import java.net.URI;

public class CaptchaAuthenticator extends UsernamePasswordForm implements Authenticator {
    protected boolean hasCookie(AuthenticationFlowContext context) {
        Cookie cookie = context.getHttpRequest().getHttpHeaders().getCookies().get("CAPTCHA_ANSWERED");
        return cookie != null;
    }

    @Override
    public void authenticate(AuthenticationFlowContext context) {
        // Cookieがある、すなわち一度Captchaを通過したデバイスは、親のActionのみ実行する
        if (hasCookie(context)) {
            super.authenticate(context);
            return;
        }
        String strategyName = context.getAuthenticatorConfig().getConfig().get("strategy");
        CaptchaStrategy strategy = CaptchaStrategies.valueOf(strategyName).getStrategy();
        strategy.challengeForm(context);
        super.authenticate(context);
    }

    @Override
    public void action(AuthenticationFlowContext context) {
        // Cookieがある、すなわち一度Captchaを通過したデバイスは、親のActionのみ実行する
        if (hasCookie(context)) {
            super.action(context);
            return;
        }
        String strategyName = context.getAuthenticatorConfig().getConfig().get("strategy");
        CaptchaStrategy strategy = CaptchaStrategies.valueOf(strategyName).getStrategy();
		if (strategy.challengeAction(context)) {
		    // CaptchaがOKであれば、親のAction(すなわちユーザ/パスワード認証)を実行する。
            super.action(context);

		    // ユーザ/パスワード認証が成功したときだけCookieをセットする。
            if (context.getStatus() == FlowStatus.SUCCESS) {
                setCookie(context);
            }
		} else {
            LoginFormsProvider form = strategy.challengeForm(context);
            form.setError(Messages.RECAPTCHA_FAILED);
            context.failureChallenge(AuthenticationFlowError.INVALID_CREDENTIALS, form.createLogin());
		}
    }


    protected void setCookie(AuthenticationFlowContext context) {
        AuthenticatorConfigModel config = context.getAuthenticatorConfig();
        int maxCookieAge = 60 * 60 * 24 * 30; // 30 days
        if (config != null) {
            String maxAge = config.getConfig().get("cookie.max.age");
            if (!maxAge.isEmpty()) {
                maxCookieAge = Integer.parseInt(maxAge);
            }
        }
        URI uri = context.getUriInfo().getBaseUriBuilder().path("realms").path(context.getRealm().getName()).build();
        addCookie(context, "CAPTCHA_ANSWERED", "true",
                uri.getRawPath(),
                null, null,
                maxCookieAge,
                false, true);
    }

    public void addCookie(AuthenticationFlowContext context, String name, String value, String path, String domain, String comment, int maxAge, boolean secure, boolean httpOnly) {
        HttpResponse response = context.getSession().getContext().getContextObject(HttpResponse.class);
        StringBuffer cookieBuf = new StringBuffer();
        // XXX Version difference おそらくsame origin
        ServerCookie.appendCookieValue(cookieBuf, 1, name, value, path, domain, comment, maxAge, secure, httpOnly);
        String cookie = cookieBuf.toString();
        response.getOutputHeaders().add(HttpHeaders.SET_COOKIE, cookie);
    }

    @Override
    public boolean requiresUser() {
        return false;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession keycloakSession, RealmModel realmModel, UserModel userModel) {

    }

    @Override
    public void close() {

    }
}

package net.unit8.keycloak.captcha;

import org.keycloak.Config;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.authentication.ConfigurableAuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CaptchaAuthenticatorFactory implements AuthenticatorFactory, ConfigurableAuthenticatorFactory {
    private static final String PROVIDER_ID = "captcha-authenticator";
    private static final CaptchaAuthenticator SINGLETON = new CaptchaAuthenticator();

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public Authenticator create(KeycloakSession keycloakSession) {
        return SINGLETON;
    }

    private static final AuthenticationExecutionModel.Requirement[] REQUIREMENT_CHOICES = {
            AuthenticationExecutionModel.Requirement.REQUIRED,
            AuthenticationExecutionModel.Requirement.ALTERNATIVE,
            AuthenticationExecutionModel.Requirement.DISABLED
    };
    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return REQUIREMENT_CHOICES;
    }

    @Override
    public boolean isUserSetupAllowed() {
        return true;
    }

    @Override
    public boolean isConfigurable() {
        return true;
    }

    private static final List<ProviderConfigProperty> configProperties = new ArrayList<>();

    static {
        ProviderConfigProperty property;

        property = new ProviderConfigProperty();
        property.setName("strategy");
        property.setLabel("Captcha Type");
        property.setHelpText("The type of captcha");
        property.setType(ProviderConfigProperty.LIST_TYPE);
        property.setOptions(Arrays.stream(CaptchaStrategies.values())
                .map(Enum::name)
                .collect(Collectors.toList()));
        configProperties.add(property);

        for (CaptchaStrategies s : CaptchaStrategies.values()) {
            CaptchaStrategy strategy = s.getStrategy();
            configProperties.addAll(strategy.getConfigProperties());
        }

        property = new ProviderConfigProperty();
        property.setName("cookie.max.age");
        property.setLabel("Cookie Max Age");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        property.setHelpText("Max age in seconds of the CAPTCHA_COOKIE.");
        configProperties.add(property);
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }

    @Override
    public String getHelpText() {
        return "A captcha when logged in first time by the device.";
    }

    @Override
    public String getDisplayType() {
        return "Captcha";
    }

    @Override
    public String getReferenceCategory() {
        return "Captcha";
    }

    @Override
    public void init(Config.Scope scope) {

    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {

    }

    @Override
    public void close() {

    }
}

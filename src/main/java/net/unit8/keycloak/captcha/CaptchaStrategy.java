package net.unit8.keycloak.captcha;

import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.provider.ProviderConfigProperty;

import java.util.List;

public interface CaptchaStrategy {
    LoginFormsProvider challengeForm(AuthenticationFlowContext context);
    boolean challengeAction(AuthenticationFlowContext context);
    List<ProviderConfigProperty> getConfigProperties();
}

package net.unit8.keycloak.captcha.strategy;

import net.unit8.keycloak.captcha.CaptchaStrategy;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.AuthenticatorConfigModel;
import org.keycloak.provider.ProviderConfigProperty;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import javax.ws.rs.core.MultivaluedMap;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

public class OxCaptchaStrategy implements CaptchaStrategy {
    private static final String MAC_ALGORITHM = "HmacSHA256";
    private final Base64.Encoder encoder;

    public OxCaptchaStrategy() {
        this.encoder = Base64.getEncoder();
    }

    private String hmac(String text, String secret) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(), MAC_ALGORITHM);
            Mac mac = Mac.getInstance(MAC_ALGORITHM);
            mac.init(secretKeySpec);
            byte[] hash = mac.doFinal(text.getBytes());
            return encoder.encodeToString(hash);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public LoginFormsProvider challengeForm(AuthenticationFlowContext context) {
        LoginFormsProvider form = context.form();
        AuthenticatorConfigModel captchaConfig = context.getAuthenticatorConfig();
        form.setAttribute("oxcaptchaRequired", true);

        OxCaptcha c = new OxCaptcha(150, 50);
        c.background();
        c.text(5);
        c.noiseCurvedLine();
        String secret = captchaConfig.getConfig().getOrDefault("hmac.secret", "This is secret");
        form.setAttribute("oxcaptchaKey", hmac(c.getText(), secret));
        BufferedImage img = c.getImage();
        try (ByteArrayOutputStream os = new ByteArrayOutputStream();
             MemoryCacheImageOutputStream mos = new MemoryCacheImageOutputStream(os)) {
            ImageIO.write(img, "png", mos);
            form.setAttribute("oxcaptchaImage", "data:image/png;base64," + encoder.encodeToString(os.toByteArray()));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return form;
    }

    @Override
    public boolean challengeAction(AuthenticationFlowContext context) {
		MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        AuthenticatorConfigModel captchaConfig = context.getAuthenticatorConfig();
        String input = formData.getFirst("oxcaptchaInput");
        String hash  = formData.getFirst("oxcaptchaKey");
        String secret = captchaConfig.getConfig().getOrDefault("hmac.secret", "This is secret");
        return Objects.equals(hash, hmac(input, secret));
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
		List<ProviderConfigProperty> properties = new ArrayList<>();

        ProviderConfigProperty property;
		property = new ProviderConfigProperty();
        property.setName("hmac.secret");
        property.setLabel("HMac Secret Key");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        property.setHelpText("The HMac secret key for OxCaptcha");
        properties.add(property);
        return properties;
    }
}

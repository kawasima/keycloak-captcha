package net.unit8.keycloak.captcha;

import net.unit8.keycloak.captcha.strategy.OxCaptchaStrategy;
import net.unit8.keycloak.captcha.strategy.ReCaptchaStrategy;

public enum CaptchaStrategies {
    RECAPTCHA(ReCaptchaStrategy.class),
    OXCAPTCHA(OxCaptchaStrategy.class);

    CaptchaStrategies(Class<? extends CaptchaStrategy> strategyClass) {
        try {
            this.strategy = strategyClass.getConstructor().newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("strategy class error", e);
        }
    }

    public CaptchaStrategy getStrategy() {
        return this.strategy;
    }
    private final CaptchaStrategy strategy;
}

# keycloak-captcha

## Description

このサンプルはKeycloakにCaptchaを設置するものです。
CaptchaにはWebサービス型のGoogle ReCaptchaと、自前で画像を生成するOxCaptchaを用いたものの2通りを切り替えて使うことができるようになっています。

KeyCloakのログインページにCapthcaを設置する上で必要なのは、

- ログインフォームを表示するときにCaptchaを表示する。
- ログインリクエストと同時に送信されるCaptchaの答えの答え合わせをする。

です。

これらはAuthenticatorを実装して、KeyCloakにデプロイすることで実現できます。
ログインページに表示するので、KeyCloakのデフォルトログインのためのAuthenticatorであるUsernamePasswordFormをextendsして使います。
これをCaptchaAuthenticatorとして実装しています。

ログインフォームを表示する処理を、`authenticate` メソッドに書きます。Captchaの種類ごとの処理は、CaptchaStrategyに委譲しています。

```java
    @Override
    public void authenticate(AuthenticationFlowContext context) {
        // Cookieがある、すなわち一度Captchaを通過したデバイスは、親のActionのみ実行する
        if (hasCookie(context)) {
            super.authenticate(context);
            return;
        }
        String strategyName = context.getAuthenticatorConfig().getConfig().get("strategy");
        CaptchaStrategy strategy = CaptchaStrategies.valueOf(strategyName).getStrategy();
        strategy.challengeForm(context);
        super.authenticate(context);
    }
```

ログインリクエストと同時に送信されるCaptchaの答えの答え合わせの処理は `action` メソッドに書きます。

```java
    @Override
    public void action(AuthenticationFlowContext context) {
        // Cookieがある、すなわち一度Captchaを通過したデバイスは、親のActionのみ実行する
        if (hasCookie(context)) {
            super.action(context);
            return;
        }
        String strategyName = context.getAuthenticatorConfig().getConfig().get("strategy");
        CaptchaStrategy strategy = CaptchaStrategies.valueOf(strategyName).getStrategy();
		if (strategy.challengeAction(context)) {
		    // CaptchaがOKであれば、親のAction(すなわちユーザ/パスワード認証)を実行する。
            super.action(context);

		    // ユーザ/パスワード認証が成功したときだけCookieをセットする。
            if (context.getStatus() == FlowStatus.SUCCESS) {
                setCookie(context);
            }
		} else {
            LoginFormsProvider form = strategy.challengeForm(context);
            form.setError(Messages.RECAPTCHA_FAILED);
            context.failureChallenge(AuthenticationFlowError.INVALID_CREDENTIALS, form.createLogin());
		}
    }
```

## Deploy

このAuthenticatorをKeycloakにデプロイします。

```
% mvn install wildfly:deploy
```

keycloakのwildflyポートはデフォルトの9990に設定してありますが、これが異なる場合はpomを書き換えてください。

## Keycloakのセットアップ

1.Themeを設定する。

ログインフォームにScript足す程度ならば、ログインフォームのテンプレートをいじらずに済みますが、Captchaの画像を表示するためのHTMLを差し込む必要
があるので、テンプレートをカスタマイズします。
src/main/resources/themeにあります。

テーマを選択するには、Realm Settings > Themesタブ >　Login Themesプルダウン で選択します。

![](https://i.gyazo.com/a097081519baa29ef2fb98f22f95f2e7.png)

2.Authentication Flowを設定する

Authentication　> Flows タブで、「Browser」フローを選択し、「copy」します。

コピーしたフローを選択し、「User name password」を削除し、「Captcha」のAuthenticatorを追加します。

![](https://i.gyazo.com/c25900b2271723eed2dbaa88adbbd5ff.png)

右端のActionsプルダウンから、Captcha Authenticatorのconfigを開きます。

![](https://i.gyazo.com/4d81305bc808fa907dd7d089f6996cef.png)

「Captcha Type」でどちらのCaptchaを使うかを選択します。Themeで選んだものと一致させてください。

ReCaptcha用には、ReCaptchaの登録時に表示されるSite KeyとSite Secretをセットします。
OxCaptchaを使う場合は、HMacの鍵文字列を設定します(適当な任意の文字列でOK)。

3.Authentication Flowを使うように設定する。

Authentication > Bindingsタブ > Browser Flowのところで、先程コピーして作ったフローを使うように設定します。

![](https://i.gyazo.com/63684b68ec5e7b3f75a4d76620daa967.png)

## Captcha回答の記録

ログインごとにCaptchaに答えるのは、コンバージョンを落とすことになるので、一度CaptchaをクリアするとCookieが仕込まれ、
同じブラウザでは以降Captchaはスキップされます。
この有効期限はAuthenticatorの設定で書き換えが可能です。
